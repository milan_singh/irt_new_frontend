import Vue from 'vue'
import Dinosaur from '@/components/Dinosaur'

describe('Dinosaur.vue', () => {
  it('should render label Age: ', () => {
    const Constructor = Vue.extend(Dinosaur)
    const vm = new Constructor().$mount()
    // console.log(vm.$el)
    expect(vm.$el.querySelector('.dinosaur p span').textContent)
    .toEqual('Age: ')
  })
})
