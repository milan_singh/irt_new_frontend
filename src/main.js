// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate'
require('./assets/styles.css')

Vue.config.productionTip = false
Vue.use(Vuelidate)
router.beforeEach((to, from, next) => {
  if (to.name !== 'LoginForm' && !localStorage.getItem('isLogged')) {
    next({name: 'LoginForm'})
  } else {
    next()
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
