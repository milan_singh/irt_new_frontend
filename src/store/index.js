import Vue from 'vue'
import Vuex from 'vuex'

import { actions } from './action'
import { mutations } from './mutations'
import { getters } from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    loginResponse: {
      status: false,
      message: ''
    }
  },
  getters,
  actions,
  mutations
})

export default store
