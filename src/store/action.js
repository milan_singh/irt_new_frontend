import axios from 'axios'

import router from '../router'

const url = 'http://localhost:3000/'

var axiosInstance = axios.create({
  baseURL: url,
  withCredentials: true
})

export const actions = {
  actionLogin: (context, payload) => {
    axiosInstance.post(url + 'user-login', payload)
    .then(function (response) {
      // context.commit('mutateLoginResponse', '')
      localStorage.setItem('isLogged', true)
      context.commit('mutateLoginResponse', { status: true, message: '' })
      router.push('/')
    })
    .catch(function (error) {
      context.commit('mutateLoginResponse', { status: false, message: errorMessageExtractor(error) })
    })
  },
  actionLogout: (context, payload) => {
    axiosInstance.post(url + 'user-logout', payload)
    .then(function (response) {
      // context.commit('mutateLoginResponse', '')
      localStorage.setItem('isLogged', '')
      context.commit('mutateLoginResponse', { status: false, message: '' })
      router.push({ name: 'LoginForm' })
    })
    .catch(function (error) {
      context.commit('mutateLoginResponse', { status: false, message: errorMessageExtractor(error) })
    })
  }
}

function errorMessageExtractor (data) {
  return data.response.data.message
}

// function responseBodyExtractor (data) {
//   return data.data
// }
